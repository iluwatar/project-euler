# https://projecteuler.net/problem=5

# Smallest multiple
# Problem 5
# 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
# What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

from itertools import count

def main():
    # generator for numbers from 1 to infinity
    gen = (x for x in count(start=1))
    # check the numbers one by one until the divisible is found
    for x in gen:
        if is_divisible(x):
            print(x)
            break
    return 0

def is_divisible(num):
    '''
    Checks if the number is evenly divisible by all of the numbers from 1 to 20
    :param num: number to check
    :return: True if divisible
    '''
    dividers = [x for x in range(1,21)]
    for d in dividers:
        if num % d != 0:
            return False
    return True

if __name__ == "__main__":
    main()
