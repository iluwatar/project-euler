# https://projecteuler.net/problem=4

# Largest palindrome product
# Problem 4
# A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
# Find the largest palindrome made from the product of two 3-digit numbers.

def main():
    largest = 0
    for i in range(100, 1000):
        for j in range(100, 1000):
            x = i * j
            if is_palindrome(x) and (x > largest):
                largest = x
    print(largest)
    return 0

def is_palindrome(x):
    '''
    Tells if the parameter converted to string is palindrome
    :param x:
    :return: True if x is palindrome
    '''
    s1 = str(x)
    s2 = str(x)[::-1]
    return s1 == s2

if __name__ == "__main__":
    main()
