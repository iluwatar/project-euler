# https://projecteuler.net/problem=1

# Multiples of 3 and 5
# Problem 1
# If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
# Find the sum of all the multiples of 3 or 5 below 1000.

from functools import reduce

def main():
    # 1. imperative style
    # result = 0
    # for i in range(1,1000):
    #     if i % 3 == 0:
    #         result += i
    #     elif i % 5 == 0:
    #         result += i
    # print(result)

    # 2. functional style
    # use list comprehesions to create a list with numbers that are multiples of 3 or 5
    multiples = [ i for i in range(1,1000) if multiple_of_3_or_5(i) ]
    # sum the list members using functools#reduce and lambda expression
    result = reduce(lambda a, x: a + x, multiples)
    print(result)

    return 0

def multiple_of_3_or_5(number):
    return (number % 3 == 0) or (number % 5 == 0)

if __name__ == "__main__":
    main()
