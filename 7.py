# https://projecteuler.net/problem=7

# 10001st prime
# Problem 7
# By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
# What is the 10 001st prime number?

import math
from itertools import count

def main():
    # we are looking for nth prime number
    nth = 10001
    # generator for numbers from 2 to infinity
    gen = (x for x in count(start=2))
    # check the numbers one by one until the nth prime is found
    num_found = 0
    for i in gen:
        if is_prime(i):
            num_found += 1
            if num_found == nth:
                print(i)
                break
    return 0

def is_prime(number):
    '''
    Determines if number is prime using trial division method
    :param number:
    :return: True if prime
    '''
    low = 2
    high = int(math.ceil(math.sqrt(number)))
    if number == 2:
        return True
    for i in range(low, high+1):
        if number % i == 0:
            return False
    return True

if __name__ == "__main__":
    main()
