# https://projecteuler.net/problem=6

# Sum square difference
# Problem 6
# The sum of the squares of the first ten natural numbers is,
# 12 + 22 + ... + 102 = 385
# The square of the sum of the first ten natural numbers is,
# (1 + 2 + ... + 10)2 = 552 = 3025
# Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.
# Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

from functools import reduce

def main():
    sum_squares = sum_of_squares(1,100)
    square_sums = square_of_sums(1,100)
    print('result = {0}'.format(square_sums - sum_squares))
    return 0

def sum_of_squares(min, max):
    '''
    Returns sum of squares for numbers from min to max (step=1)
    :param min: minimum (including)
    :param max: maximum (including)
    :return: sum of squares
    '''
    squares = list(map(lambda x: x*x, [n for n in range(min, max+1)]))
    result = reduce(lambda a,x: a+x, squares)
    return result

def square_of_sums(min, max):
    '''
    Return square of sums for numbers from min to max (step=1)
    :param min: minimum (including)
    :param max: maximum (including)
    :return: square of sums
    '''
    sum = reduce(lambda a,x: a+x, [n for n in range(min, max+1)])
    result = sum * sum
    return result

if __name__ == "__main__":
    main()
