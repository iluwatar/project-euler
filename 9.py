# https://projecteuler.net/problem=9

# Special Pythagorean triplet
# Problem 9
# A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
#
# a^2 + b^2 = c^2
# For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
#
# There exists exactly one Pythagorean triplet for which a + b + c = 1000.
# Find the product abc.

def main():
    maximum = 1001
    a = [i for i in range(1, maximum)]
    for n in a:
        b = [j for j in range(n+1, maximum)]
        for m in b:
            t = euclid_solver((m, n))
            print('n={} m={} a={} b={} c={} sum={}'.format(n, m, t[0], t[1], t[2], tuple_sum(t)))
            if tuple_sum(t) == 1000:
                print("found it, product a*b*c={}".format(tuple_product(t)))
                break
        else:
            continue # executed if the loop ended normally (no break)
        break # executed if 'continue' was skipped (break)
    return 0

def euclid_solver(pair):
    '''
    Euclid's formula is a fundamental formula for generating Pythagorean triples given an arbitrary pair of positive
    integers m and n with m > n. The formula states that the integers
    a = m^2 - n^2 , b = 2mn , c = m^2 + n^2
    :param in: tuple containing m, n
    :return: tuple containing a, b, c solved
    '''
    a = pair[0]*pair[0] - pair[1]*pair[1]
    b = 2*pair[0]*pair[1]
    c = pair[0]*pair[0] + pair[1]*pair[1]
    return a, b, c

def tuple_sum(t):
    '''
    Returns sum of tuple tokens
    :param t: tuple
    :return: sum
    '''
    result = 0
    for x in range(len(t)):
        result += int(t[x])
    return result

def tuple_product(t):
    '''
    Returns product of tuple tokens
    :param t: tuple
    :return: product
    '''
    result = 1
    for x in range(len(t)):
        result *= int(t[x])
    return result

if __name__ == "__main__":
    main()
