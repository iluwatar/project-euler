# https://projecteuler.net/problem=3

# Largest prime factor
# Problem 3
# The prime factors of 13195 are 5, 7, 13 and 29.
# What is the largest prime factor of the number 600851475143 ?

import math

def main():
    number = 600851475143
    # the number is not prime, so its largest prime factor must be <= sqrt(number)
    root = int(math.ceil(math.sqrt(number)))
    # calculate primes up to root
    primes = sieve_of_eratosthenes(root)[::-1]
    # test to find out largest prime factor
    for prime in primes:
        if number % prime == 0:
            print('result={0}'.format(prime))
            break
    return 0

def sieve_of_eratosthenes(n):
    '''
    Calculates prime numbers up to n
    :param n: the roof for prime number calculation
    :return: list of prime numbers
    '''
    # initialize
    marks = {i: False for i in range(2, n)}
    # apply algorithm
    for i in range(2, n):
        if marks[i]:
            continue
        x = i + i
        while x < n:
            marks[x] = True
            x += i
    # create result based on marked elements
    primes = [i for i in range(2, n) if not marks[i]]
    return primes

if __name__ == "__main__":
    main()
